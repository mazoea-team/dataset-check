# -*- coding: utf-8 -*-
import argparse
import sys
import shutil
import os
import glob
import logging
import json
try:
    import yaml
except Exception:
    print("Install yaml library `pip install pyyaml` or update PYTHONPATH")
    sys.exit(1)
import mazutils
import _imgproc as imgproc

# =====================================
from datetime import datetime
# =====================================
timestamp = datetime.now().strftime("%Y_%m_%d_%H_%M_%S")
log_dir = None
_this_dir = os.path.dirname(os.path.abspath(__file__))
_logger = logging.getLogger('test_model')


# =====================================

def get_affiliates(f, affiliates, exts):
    """
        Get other files to show.
    """
    f_other = []
    for aff in affiliates:
        f_base = None
        for ext in exts:
            if f.endswith(ext):
                f_base = f[:-len(ext)]
                break
        if f_base is not None:
            f_other.append(f_base + aff)
    for f in f_other:
        if not os.path.exists(f):
            _logger.critical('Cannot find [%s]', f)
            return []
    return f_other


def load_gt(file_path, read_only=False):
    result = {
        'data': {},
        'updated': [],
        'time': ts,
    }

    if os.path.exists(file_path):
        _logger.info('Loading [%s]', file_path)
        with open(file_path, mode='r') as fin:
            result = json.load(fin)
            if not read_only:
                result['updated'].append(result['time'])
                result['time'] = ts
        _logger.info('Loaded [%d] files updated at [%s]',
                     len(result['data']), result['time'])

    return result


# =====================================

def info(args, ts):
    result = load_gt(args.output, True)
    _logger.info('Created [%s] with [%d] data items',
                 result['time'], len(result['data']))
    from collections import defaultdict
    d = defaultdict(list)
    for k, v in result['data'].items():
        d[v['tag']].append(k)
    msg = '\n'
    for k, v in d.items():
        msg += '%20s: [%4d]\n' % (k, len(v))
    _logger.info(msg)
    #
    if args.info_norm:
        _logger.info('Normalizing')
        norm_data = {}
        for k, v in result['data'].items():
            norm_k = k.split('.')[0]
            norm_data[norm_k] = v
        result['data'] = norm_data
        with open(args.output, mode='w+') as fout:
            json.dump(result, fout, sort_keys=True, indent=2)


def fix(args, ts):
    with open(args.config, mode="r") as fin:
        gcfg = yaml.safe_load(fin)
    files = []
    ext = gcfg['fix_command']['ext']
    files += glob.glob(os.path.join(args.dataset, '*.' + ext))
    _logger.info('Found [%d] files in [%s]', len(files), args.dataset)

    if not os.path.exists(args.output):
        _logger.critical('Expecting [%s] to be present', args.output)
        sys.exit(1)

    result = load_gt(args.output)
    result_fixed = {}
    fix_tag = args.fix.lower()
    cmd_g = gcfg['fix_command']['cmd']
    # fix keys
    for k, v in result['data'].items():
        result_fixed[k.strip(gcfg['fix_command']['strip'])] = v

    if 0 == len(args.output_dir):
        _logger.critical('Missing --output-dir parameter')
        sys.exit(1)
    if not os.path.exists(args.output_dir):
        os.makedirs(args.output_dir)

    # show only
    for i, f in enumerate(files):
        base_f = os.path.basename(f)
        base_f = os.path.splitext(base_f)[0]
        base_f = base_f.strip(ext)
        if base_f not in result_fixed:
            _logger.critical('File [%s] not found in gt', base_f)
            continue
        tag = result_fixed[base_f].get('tag', '') or ''
        if fix_tag != tag.lower():
            continue
        _logger.info('Working on [%s]', base_f)

        new_f = os.path.join(args.output_dir, base_f + '.png')
        if os.path.exists(new_f):
            _logger.info('Skipping [%s]', new_f)
            continue

        shutil.copy(f, new_f)
        cmd = cmd_g % new_f
        try:
            ret, stdout, stderr, took = mazutils.run_smart_os({}, cmd)
            _logger.info((stdout or '') + (stderr or ''))
        except Exception as e:
            _logger.exception('Invalid cmd [%s]', cmd)


def check(args, ts):
    """
        1. Show files
        2. Get tag
        3. output tag
    """
    with open(args.config, mode="r") as fin:
        gcfg = yaml.safe_load(fin)
    files = []
    exts = gcfg['exts']
    for ext in exts:
        files += glob.glob(os.path.join(args.dataset, '*.' + ext))
    _logger.info('Found [%d] files in [%s]', len(files), args.dataset)
    tags = gcfg['tags']
    _logger.info('Using [%s] tags', tags)

    result = load_gt(args.output)
    aff_exts = gcfg.get('affiliates', [])

    for i, f in enumerate(files):
        base_f = os.path.basename(f).split('.')[0]

        if args.skip_done:
            if base_f in result['data']:
                continue

        f_aff = get_affiliates(f, aff_exts, exts)
        # show all files
        f_aff_img = []
        for f_a in f_aff:
            f_a_img = imgproc.loadImage(f_a)
            f_aff_img.append(f_a_img)
        f_img = imgproc.loadImage(f)

        if gcfg.get('bbox_gt', False):
            bbox_txt = os.path.splitext(f)[0] + '.txt'
            if os.path.exists(bbox_txt):
                import gt
                gt = gt.txt(bbox_txt)
                f_img = imgproc.show_bboxes_poly(
                    f_img, [x[0] for x in gt.line_bb_txt])

        #
        cur_info = {
            'tag': None,
            'previous': [],
        }
        if base_f in result['data']:
            cur_info = result['data'][base_f]
        else:
            result['data'][base_f] = cur_info
        _logger.info('[%4d/%4d] Working on [%s] with tag [%s]',
                     i, len(files), base_f, cur_info['tag'])

        tag = imgproc.get_tag([f_img] + f_aff_img, tags,
                              previous_tag=cur_info['tag'], title=base_f)
        tag = tag[0] if tag is not None and 0 < len(tag) else None
        _logger.critical('[%s] tagged with [%s]', base_f, tag)

        # store previous if any
        if cur_info['tag'] is not None and cur_info['tag'] != tag:
            cur_info['previous'].append(cur_info['tag'])

        cur_info['previous'] = list(set(cur_info['previous']))

        # store current
        cur_info['tag'] = tag

        # store after each run
        with open(args.output, mode='w+') as fout:
            json.dump(result, fout, sort_keys=True, indent=2)

    # =====================================


if __name__ == "__main__":
    ts = mazutils.ts()
    _logger = mazutils.init_logger(ts, log_to_file=True)
    parser = argparse.ArgumentParser(description='Dataset checker')
    parser.add_argument('--dataset', type=str,
                        help='Path to image directory', default='')
    parser.add_argument('--output',
                        default=os.path.join(_this_dir, '../dataset.gt.json'),
                        type=str, help='Output file name')
    parser.add_argument('--output-dir',
                        default='', type=str, help='Output dir name (for `fix`)')
    parser.add_argument('--config',
                        default=os.path.join(_this_dir, os.path.join(
                            _this_dir, '../.dataset.yaml')),
                        type=str, help='Configuration file path')
    parser.add_argument('--skip_done',
                        default=False, action='store_true', help='Continue files')
    parser.add_argument('--fix',
                        default='', type=str, help='Fix specific tag')
    parser.add_argument('--info_only',
                        default=False, action='store_true', help='Print info only')
    parser.add_argument('--info_norm',
                        default=False, action='store_true', help='Normalize')

    args = parser.parse_args()
    if os.path.exists(args.config) is False:
        _logger.critical('Path [%s] does not exist', args.config)
        sys.exit(1)
    if not args.info_only and os.path.exists(args.dataset) is False:
        _logger.critical('Path [%s] does not exist', args.dataset)
        sys.exit(1)

    if args.info_only:
        info(args, ts)

    elif 0 < len(args.fix):
        fix(args, ts)

    else:
        check(args, ts)
