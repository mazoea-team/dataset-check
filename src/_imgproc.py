# -*- coding: utf-8 -*-
import random
import os
import logging
import numpy as np
from skimage import io
import cv2
import matplotlib
matplotlib.use('TkAgg')
from matplotlib.widgets import Button
from matplotlib import pyplot as plt
logging.getLogger('matplotlib.font_manager').disabled = True


def show(img, title, wait=True):
    cv2.imshow(title, img)
    if wait:
        cv2.waitKey()


def maximize():
    backend = plt.get_backend()
    mng = plt.get_current_fig_manager()

    # # windows
    # try:
    #     mng.window.state('zoomed')
    # except Exception as e:
    #     pass

    # ubuntu (specific backend)
    if backend == 'TkAgg':
        try:
            mng.resize(*mng.window.maxsize())
            mng.window.wm_geometry("+0+0")
            mng.window.state('zoomed')
            return
        except Exception as e:
            pass
    #
    # try:
    #     mng.frame.Maximize(True)
    # except Exception as e:
    #     pass
    #
    # try:
    #     mng.window.maximize()
    # except Exception as e:
    #     pass

    if backend == 'GTK3Agg':
        try:
            mng.full_screen_toggle()
            pass
        except Exception as e:
            pass


def get_tag(img_arr, tags, previous_tag=None, title=None):
    global clicked_tag
    fig = plt.figure(figsize=(6, 6), tight_layout=True, num=title)
    fig.canvas.toolbar.set_message = lambda x: ''
    plt.subplots_adjust(hspace=0, wspace=0)
    clicked_tag = []
    columns = len(img_arr)
    rows = 1
    plt.xticks([])
    plt.yticks([])
    for i in range(0, columns * rows):
        ax = fig.add_subplot(rows, columns, 1 + i)
        plt.setp(ax.get_xticklabels(), visible=False)
        plt.setp(ax.get_yticklabels(), visible=False)
        plt.imshow(img_arr[i])

    maximize()

    dim = 1.0 / len(tags)
    h = 0.025
    but_tag = []
    for i, tag in enumerate(tags):
        axcut = plt.axes([i * dim, 1.0 - h, dim, h])
        but_tag.append((axcut, tag))
        if previous_tag == tag:
            Button(axcut, tag, color='red')
        else:
            Button(axcut, tag)

    def on_click(e, ret_value):
        for b, tag in but_tag:
            if e.inaxes == b:
                ret_value.append(tag)
                plt.close()
                return

    cid = plt.connect('button_press_event', lambda x: on_click(x, clicked_tag))
    plt.show()
    try:
        # plt.disconnect(cid)
        pass
    except:
        pass

    return clicked_tag

    # cv2.namedWindow(title, cv2.WINDOW_NORMAL)
    # cv2.imshow(title, img)
    # if wait:
    #     cv2.waitKey()


def loadImage(img_file):
    img = io.imread(img_file)
    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)
    if img.shape[0] == 2:
        img = img[0]
    if len(img.shape) == 2:
        img = cv2.cvtColor(img, cv2.COLOR_GRAY2RGB)
    if img.shape[2] == 4:
        img = img[:, :, :3]
    img = np.array(img)

    return img


# =====================================

def get_w_h(bb):
    xmin = bb[:, 0].min()
    xmax = bb[:, 0].max()
    ymin = bb[:, 1].min()
    ymax = bb[:, 1].max()
    width = xmax - xmin
    height = ymax - ymin
    return width, height


def get_min_max(bb):
    xmin = bb[:, 0].min()
    xmax = bb[:, 0].max()
    ymin = bb[:, 1].min()
    ymax = bb[:, 1].max()
    return xmin, xmax, ymin, ymax


def resize(img, mag):
    new_dim = (int(img.shape[1] * mag), int(img.shape[0] * mag))
    return cv2.resize(img, new_dim, interpolation=cv2.INTER_AREA)


def to_bb(arr):
    return np.array([
        [arr[0], arr[1]],
        [arr[2], arr[3]],
        [arr[4], arr[5]],
        [arr[6], arr[7]],
    ], np.int32)


def to_bb4(arr):
    return np.array([
        [arr[0], arr[1]],
        [arr[2], arr[1]],
        [arr[2], arr[3]],
        [arr[0], arr[3]],
    ], np.int32)


def show_bboxes_poly(image, bboxes, thickness=3, no_random=False):
    for i in range(len(bboxes)):
        # BGR
        c = (
            random.randint(50, 205) if not no_random else 100,
            random.randint(50, 205) if not no_random else 100,
            0
        )
        pts = np.reshape(np.int32(bboxes[i]), (-1, 1, 2))
        cv2.polylines(image, [pts], True, c, thickness=thickness)
    return image


def get_bboxes(cnt, stats, min_area):
    rects = []
    for i in range(1, cnt):
        x, y = stats[i, cv2.CC_STAT_LEFT], stats[i, cv2.CC_STAT_TOP]
        w, h = stats[i, cv2.CC_STAT_WIDTH], stats[i, cv2.CC_STAT_HEIGHT]
        if w * h < min_area:
            continue
        rects.append(to_bb([x, y, x + w, y, x + w, y + h, x, y + h]))
    rects.sort(key=lambda x: x[0][0])
    return rects


def cc_get_letters_bboxes(word_image, debug=0):
    gray = word_image if len(word_image.shape) != 3 else cv2.cvtColor(
        word_image, cv2.COLOR_RGB2GRAY)
    ret, word_binary_bin = cv2.threshold(
        gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)
    if 1 < debug:
        show(word_binary_bin, 'word_binary_bin', False)
    # for letters
    nb_letter_components, _, letter_stats, _ = cv2.connectedComponentsWithStats(
        word_binary_bin, connectivity=8)

    img_h, img_w = word_image.shape[0:2]
    letter_rects = get_bboxes(nb_letter_components, letter_stats, img_h / 5 * 3)

    if 1 < debug:
        word_image_letter_dbg = show_bboxes_poly(
            word_image.copy(), letter_rects)
        mag = 4
        show(resize(word_image_letter_dbg, mag), 'word_image_letter_dbg', False)

    return letter_rects


def cc_get_word_bboxes(word_image, k, debug=0):
    gray = word_image if len(word_image.shape) != 3 else cv2.cvtColor(
        word_image, cv2.COLOR_RGB2GRAY)

    ret, word_binary_bin = cv2.threshold(
        gray, 0, 255, cv2.THRESH_BINARY_INV + cv2.THRESH_OTSU)

    # for text
    kernel = np.ones((3, k), np.uint8)
    word_binary_bin_closed = cv2.morphologyEx(
        word_binary_bin, cv2.MORPH_CLOSE, kernel, iterations=1)
    if 1 < debug:
        show(word_binary_bin_closed, 'word_binary_bin_closed', False)
    nb_word_components, _, word_stats, _ = cv2.connectedComponentsWithStats(
        word_binary_bin_closed, connectivity=8)

    img_h, img_w = word_image.shape[0:2]
    word_rects = get_bboxes(nb_word_components, word_stats, img_h / 5 * 3)

    if 1 < debug:
        word_image_word_dbg = show_bboxes_poly(word_image.copy(), word_rects)
        mag = 4
        show(resize(word_image_word_dbg, mag), 'word_image_word_dbg', False)

    return word_rects


def bboxes_x_overlap(bboxes, min_overlap):
    for i in range(0, len(bboxes) - 1):
        l1 = np.min(bboxes[i][:, 0])
        r1 = np.max(bboxes[i][:, 0])
        l2 = np.min(bboxes[i + 1][:, 0])
        r2 = np.max(bboxes[i + 1][:, 0])
        if r1 < l2 or r2 < l1:
            continue
        # more than 75% overlap
        perc_overlap = (
            100. * (min(r1, r2) - max(l1, l2))) / min(r2 - l2, r1 - l1)
        if min_overlap <= perc_overlap:
            return True
    return False


def bboxes_y_overlap(bboxes, min_overlap):
    for i in range(0, len(bboxes) - 1):
        l1 = np.min(bboxes[i][:, 1])
        r1 = np.max(bboxes[i][:, 1])
        l2 = np.min(bboxes[i + 1][:, 1])
        r2 = np.max(bboxes[i + 1][:, 1])
        if r1 < l2 or r2 < l1:
            continue
        # more than 75% overlap
        perc_overlap = (
            100. * (min(r1, r2) - max(l1, l2))) / min(r2 - l2, r1 - l1)
        if min_overlap <= perc_overlap:
            return True
    return False


def proper_bb(bb):
    tl, tr, br, bl = bb[0:4]
    # top x
    if tr[0] < tl[0] + 1:
        return False
    if br[0] < bl[0] + 1:
        return False

    if bl[1] < tl[1] + 1:
        return False
    if br[1] < tr[1] + 1:
        return False

    return True


def debug_img(outputdir, name, img):
    out_dir = os.path.join(outputdir, 'debug')
    if not os.path.exists(out_dir):
        os.makedirs(out_dir)
    out_f = os.path.join(out_dir, name)
    cv2.imwrite(out_f, img)
    return out_f
