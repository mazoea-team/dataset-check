import codecs
import locale
import subprocess
import sys
import os
import logging
import tempfile
import threading
import time
from datetime import datetime, timedelta
from functools import wraps

PY2 = (2 == sys.version_info[0])

success = 0
code_timeout = -4242
code_error = -123

try:
    _basestring = basestring
except Exception:
    _basestring = str
_this_dir = os.path.dirname(os.path.abspath(__file__))
_logger = logging.getLogger('mazutils')


def ts():
    return datetime.now().strftime("%Y_%m_%d_%H_%M_%S")


def init_logger(timestamp, log_dir=None, log_to_default=False, log_to_file=False, logging_level_all=logging.DEBUG):
    if log_dir is None or log_to_default is True:
        log_dir = os.path.join(_this_dir, "__logs")

    if not os.path.exists(log_dir):
        os.makedirs(log_dir)

    logging_level = logging.INFO
    log_fmt = logging.Formatter('[%(asctime)s] %(levelname)s - %(message)s')
    logger = logging.getLogger()
    logger.setLevel(logging_level_all)
    _logfile = os.path.join(log_dir, '%s.log' % timestamp)

    if log_to_file:
        handlers = [
            (logging.FileHandler(_logfile, 'a+', 'utf-8'), logging_level_all),
            (logging.StreamHandler(), logging_level),
        ]
    else:
        handlers = [
            (logging.StreamHandler(), logging_level_all),
        ]

    for h, lvl in handlers:
        h.setLevel(lvl)
        h.setFormatter(log_fmt)
        logger.addHandler(h)

    pil_logger = logging.getLogger('PIL')
    pil_logger.setLevel(logging.INFO)

    return logger


def safe_unlink(file_path):
    """
        Safe delete of a file looping up to 1 sec.

        .. note::

          On some systems (i.e., Windows) the process can still `somehow` have the file opened.
    """
    assert isinstance(file_path, _basestring)
    max_loops = 10
    while 0 < max_loops:
        try:
            os.unlink(file_path)
            return
        except Exception as e:
            time.sleep(0.1)
            if max_loops == 1:
                _logger.warning(
                    u"Could not remove file [%s] [%s]",
                    file_path, repr(e)
                )
        finally:
            max_loops -= 1


def os_windows():
    """
        @return: True if the os is Windows-like.
    """
    import platform
    return u"windows" in platform.platform().lower()


def run(env_dict, cmd, logger=None, debug=0, env=None, cwd=None, timeout=None):
    """
        Run local process using command line in cmd.

        Returns

          return code
          stdout
          stderr
          took time
    """
    took = 0.
    start = time.time()
    temp_dir = env_dict.get(u"temporary_directory", u"") or None

    tempik_stdout = tempfile.NamedTemporaryFile(
        mode=u"w+b",
        suffix=u".cmd.stdout.txt",
        dir=temp_dir,
        delete=False
    )
    tempik_stderr = tempfile.NamedTemporaryFile(
        mode=u"w+b",
        suffix=u".cmd.stderr.txt",
        dir=temp_dir,
        delete=False
    )
    if logger:
        logger.debug(u"Running [%s] into\n [%s] [%s]",
                     cmd,
                     tempik_stdout.name,
                     tempik_stderr.name)
    try:
        if PY2:
            cmd = cmd.encode(locale.getpreferredencoding())
    except Exception:
        pass

    p = subprocess.Popen(cmd,
                         shell=True,
                         stdin=None,
                         stdout=tempik_stdout,
                         stderr=tempik_stderr,
                         cwd=cwd,
                         env=env)
    timer = None
    timer_reached = [False]
    if timeout is not None:
        def _finish():
            p.terminate()
            timer_reached[0] = True

        timer = threading.Timer(float(timeout), _finish)
        timer.start()

    # wait for the end
    p.communicate()
    if timer is not None:
        timer.cancel()

    tempik_stdout.close()
    tempik_stderr.close()

    took = round(time.time() - start, 2)
    ret = [None, None]
    try:
        with codecs.open(tempik_stdout.name, u"r", u"utf-8", errors=u"replace") as ftemp:
            ret[0] = ftemp.read().strip()
        with codecs.open(tempik_stderr.name, u"r", u"utf-8", errors=u"replace") as ftemp:
            ret[1] = ftemp.read().strip() or None
    except Exception as e:
        _logger.critical(u"Run problem with [%s][%s][%s]", cmd, ret[0], uni(e))

    safe_unlink(tempik_stdout.name)
    safe_unlink(tempik_stderr.name)

    ret_code = p.returncode
    if timer_reached[0]:
        ret_code = code_timeout

    return ret_code, ret[0], ret[1], took


def run_smart_os(env_dict, cmd, logger=None, cwd=None, timeout=None):
    """
        Try to be os independent.

        Returns the same as utils.run.
    """
    if "$" in cmd and logger is not None:
        logger.warning(
            u"Probably not all arguments have been successfully replaced - [%s]", cmd)
    if not os_windows():
        cmd = cmd.replace(u"\\", u"/")
    return run(env_dict, cmd, logger, cwd=cwd, timeout=timeout)


def create_dirs(d):
    if os.path.exists(d):
        return
    os.makedirs(d)


def nice_sec(s):
    return str(timedelta(seconds=s))


def git_short_hash():
    """ Return git short hash """
    the_format = u"%h - %ai"
    ret = subprocess.Popen(
        [u'git', u'log', u'-1', u'--format=%s' % the_format],
        stdout=subprocess.PIPE
    ).communicate()[0].strip()
    return ret if PY2 else ret.decode("utf-8")


def git_diff():
    """ Return git diff """
    the_format = u"%h - %ai"
    ret = subprocess.Popen(
        [u'git', u'diff', u'-1', u'--format=%s' % the_format],
        stdout=subprocess.PIPE
    ).communicate()[0].strip()
    return ret if PY2 else ret.decode("utf-8")


def log_diff(logger, git_repo):
    try:
        cur_dir = os.getcwd()
        os.chdir(git_repo)
        logger.info("Repo info: [%s]", git_short_hash())
        logger.info("Repo diff: [%s]", git_diff()[:5000])
    finally:
        os.chdir(cur_dir)


def timing(f):
    @wraps(f)
    def wrap(*args, **kw):
        ts = time.time()
        result = f(*args, **kw)
        te = time.time()
        _logger.info('[%r] took: %2.4f seconds' % (f.__name__, te - ts))
        return result
    return wrap
