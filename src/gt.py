# -*- coding: utf-8 -*-
import logging
import _imgproc as imgproc
import numpy as np

_logger = logging.getLogger('gt')


class txt(object):

    def __init__(self, file_path):
        self.line_bb_txt = []
        with open(file_path, mode='r') as fin:
            buf = fin.read()  # .decode('utf-8')
            lines = buf.splitlines()
            # `72,25,326,25,326,64,72,64,TAN WOON YANN`
            for l in lines:
                l = l.strip()
                if 0 == len(l):
                    continue
                splits = l.split(',', maxsplit=8)
                if len(splits) != 9:
                    _logger.critical('Invalid GT [%s]', file_path)
                    raise
                self.line_bb_txt.append((imgproc.to_bb(splits[:8]), splits[8]))

    @staticmethod
    def write(file_path, word_bb_arr):
        with open(file_path, mode='w+') as fout:
            # `72,25,326,25,326,64,72,64,TAN WOON YANN`
            for bb, t in word_bb_arr:
                arr = np.ravel(bb)
                bb_txt = ''
                for i in range(arr.shape[0]):
                    bb_txt += '%d,' % arr[i]
                bb_txt = bb_txt[:-1]
                l = '%s,%s\n' % (bb_txt, t)
                fout.write(l)
