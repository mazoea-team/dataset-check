# 

```
pip3 install -r requirements.txt
apt get install python3-tk 
```

# Normal run
```
python main.py --dataset=...
```

# Show info from created dataset
```
python main.py --info_only
```

Show info and normalize keys
```
python main.py --info_only --output=d:\data\invoices\dataset.invoices.gt.json --info_norm
```


# Skip done
```
python main.py --skip_done
```

# Fix only images with specific tag (read only)
```
python main.py --dataset=d:\data\invoices-v2\debug\ --fix=OK --output-dir=d:\data\invoices-v2\images-fixed\
```